package com.cp.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cp.model.User;
import com.cp.service.UserService;

@Controller
public class UserController {

	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String home(Locale locale, Model model){
		
		System.out.println("Home Page Request, Locale = "+locale);
		
		Date date = new Date();
		
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG,locale);
		String formattedDate = dateFormat.format(date);
		System.out.println(formattedDate);
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String user() {
		return "login";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String user(@Validated User user, Model model,HttpSession session) {
		System.out.println("User Page Requested"+user.getEmail());
		User quser = userService.findUser(user);
		System.out.println(quser);
		if(null != quser){
			if(quser.getPassword().equals(user.getPassword())){
				session.setAttribute("user", quser);
				return "welcome";
			}
		}
		return "login";
		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(@Validated User user, Model model){
		System.out.println("register");
		model.addAttribute("regiser", user);
		return "register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@Validated User user, Model model){
		User existUser = userService.findUser(user);
		if(null == existUser){
			userService.saveUser(user);
			model.addAttribute("user", "reg");
		}else{
			model.addAttribute("user", user);
		}
		return "login";
	}
	
	@RequestMapping(value = "/viewall", method = RequestMethod.GET)
	public String viewall(){
		return "viewall";
	}
	
}
