package com.cp.dao;

import com.cp.model.User;

public interface UserDao {

	User findUser(User user);

	boolean saveUser(User user);

	
	
}
