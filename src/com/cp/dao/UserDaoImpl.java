package com.cp.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.cp.model.User;
import com.cp.rowmapper.UserRowMapper;

@Repository
public class UserDaoImpl implements UserDao {

	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/*public void setTemplate(JdbcTemplate jdbcTemplate) {    
	    this.jdbcTemplate = jdbcTemplate;    
	}*/

	@Override
	public User findUser(User user) {
		
		System.out.println("SELECT * from USER where email = '"+user.getEmail()+"'");
		String query = "SELECT * from USER where email = '"+user.getEmail()+"'";
		List<User> name = jdbcTemplate.query(query, new UserRowMapper());
		if(null != name && name.size() != 0){
			System.out.println("user exist");
			return name.get(0);

		}
		return null;  
	}

	@Override
	public boolean saveUser(User user) {
		String query = "INSERT INTO USER (name,email,password,age) VALUES (?,?,?,?);";
		int i = 0;
		i = jdbcTemplate.update(query, new Object[]{
			user.getName(),user.getEmail(),user.getPassword(),user.getAge()
		});
		if(i != 0){
			System.out.println("Query OK,"+i+" row affected");
		}
		return false;
	}    
}
 