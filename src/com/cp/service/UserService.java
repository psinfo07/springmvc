package com.cp.service;

import com.cp.model.User;

public interface UserService {

	User findUser(User user);

	boolean saveUser(User user);

	
}
