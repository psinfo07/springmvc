package com.cp.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.cp.dao.UserDao;
import com.cp.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	
	@Override
	public User findUser(User user) {
		
		return userDao.findUser(user);
	}


	@Override
	public boolean saveUser(User user) {
		return userDao.saveUser(user);
	}

}
